# JIRA Service Desk - Custom Reports add-on example.

Welcome to this add-on example.

This add-on example shows you how you can add Custom Reports to the JIRA Service Desk Agent Portal.

We are using the ACE ([Atlassian Connect Express](https://bitbucket.org/atlassian/atlassian-connect-express)) framework. 

## Want to know more about custom reports in JIRA Service Desk?

[Adding your Custom Reports to JIRA Service Desk](https://developer.atlassian.com/jiracloud/jira-service-desk-modules-agent-view-39988009.html#JIRAServiceDeskmodules-Agentview-Reportgroup).